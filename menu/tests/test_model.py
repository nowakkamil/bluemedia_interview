from django import test

from menu import models


class MenuCardTest(test.TestCase):
    def test_str(self):
        obj = models.MenuCard(
            name='Test name',
            description='Test description',
        )
        self.assertEqual(str(obj), 'Test name')
