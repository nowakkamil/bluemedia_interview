import factory
import factory.fuzzy

from menu import models


class MenuCardFactory(factory.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText(length=50)
    description = factory.fuzzy.FuzzyText(length=200)

    class Meta:
        model = models.MenuCard


class DishFactory(factory.DjangoModelFactory):
    name = factory.fuzzy.FuzzyText(length=50)
    description = factory.fuzzy.FuzzyText(length=200)
    preparation_time = factory.fuzzy.FuzzyInteger(low=0, max=100)
    menu_card = factory.SubFactory(MenuCardFactory)

    class Meta:
        model = models.Dish
