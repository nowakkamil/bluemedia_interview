import json

from rest_framework import test

from menu import models


class ApiTestCase(test.APITestCase):
    def test_menu_card_without_dishes_should_not_be_returned(self):
        models.MenuCard.objects.create(
            name='Test name',
            description='Test description',
        )
        response = self.client.get('/api/menus/')
        self.assertEquals(len(response.data), 0)

    def test_menu_card_with_dishes(self):
        menu = models.MenuCard.objects.create(
            name='Test name',
            description='Test description',
        )
        models.Dish.objects.create(
            name='Test name dish',
            description='Test description dish',
            menu_card=menu
        )
        response = self.client.get('/api/menus/')
        self.assertEquals(len(response.data), 1)

    def test_menu_list(self):
        menu = models.MenuCard.objects.create(
            name='Test name',
            description='Test description',
        )
        models.Dish.objects.create(
            name='Test name dish',
            description='Test description dish',
            menu_card=menu
        )
        response = self.client.get('/api/menus/')
        self.assertEquals(len(response.data), 1)
        self.assertEquals(
            json.dumps(response.data),
            '[{"id": 3, "name": "Test name", "description": "Test description", "dishes_count": 1}]'
        )

    def test_menu_retrieve(self):
        menu = models.MenuCard.objects.create(
            name='Test name',
            description='Test description',
        )
        models.Dish.objects.create(
            name='Test name dish',
            description='Test description dish',
            menu_card=menu
        )
        response = self.client.get(f'/api/menus/{menu.id}/')
        self.assertEquals(
            json.dumps(response.data),
            '{"id": 4, "name": "Test name", "description": "Test description", "dishes": '
            '[{"id": 3, "name": "Test name dish", "description": "Test description dish", "preparation_time": 0, '
            '"is_vegetarian": false}]}'
        )
