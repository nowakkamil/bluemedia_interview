from django.contrib import admin

from menu import models


class MenuCardAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')


class DishAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'preparation_time', 'menu_card', 'is_vegetarian')


admin.site.register(models.MenuCard, MenuCardAdmin)
admin.site.register(models.Dish, DishAdmin)
