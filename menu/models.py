from django.db import models


class MenuCard(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255, unique=True)
    description = models.TextField()

    def __str__(self):
        return self.name


class Dish(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255)
    description = models.TextField()
    preparation_time = models.IntegerField(default=0)
    menu_card = models.ForeignKey(MenuCard, related_name='dishes')
    is_vegetarian = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Dishes'
