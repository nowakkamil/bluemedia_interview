from django.test import TestCase
from django.urls import reverse


class URLsTest(TestCase):

    def test_home_page(self):
        url = reverse('admin:index', args=[])
        self.assertEqual(url, '/admin/')

    def test_api_page(self):
        url = reverse('api:api-root', args=[])
        self.assertEqual(url, '/api/')

    def test_menus_page(self):
        url = reverse('api:menu-list')
        self.assertEqual(url, '/api/menus/')


