from rest_framework import viewsets

from api.serializers import MenuSerializer, MenuSimpleSerializer
from menu.models import MenuCard


class MenuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MenuCard.objects.exclude(dishes=None)

    def get_serializer_class(self):
        if self.action is 'retrieve':
            return MenuSerializer
        else:
            return MenuSimpleSerializer
