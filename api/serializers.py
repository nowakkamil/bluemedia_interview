from rest_framework import serializers

from menu.models import MenuCard
from menu.models import Dish


class DishSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dish
        fields = ('id', 'name', 'description', 'preparation_time', 'is_vegetarian')


class MenuSerializer(serializers.ModelSerializer):
    dishes = DishSerializer(many=True)

    class Meta:
        model = MenuCard
        fields = ('id', 'name', 'description', 'dishes')


class MenuSimpleSerializer(serializers.ModelSerializer):
    dishes_count = serializers.SerializerMethodField()

    def get_dishes_count(self, obj):
        return obj.dishes.count()

    class Meta:
        model = MenuCard
        fields = ('id', 'name', 'description', 'dishes_count')

