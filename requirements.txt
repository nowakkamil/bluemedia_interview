Django==1.11.3
psycopg2==2.7.1
django-cors-headers==2.1.0
djangorestframework==3.6.3
factory_boy