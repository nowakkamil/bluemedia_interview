# Interview project

# FRONTEND Angular 2 app

This project was generated with [angular-cli](https://github.com/angular/angular-cli).

### Install Angular CLI globally

Run `npm install -g angular-cli`

### Install NPM dependencies

Navigate to `frontend` directory and:

    npm install

### Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


# BACKEND Django app

### Requirements and dependencies
* Python 3.6


### Setting up virtualenv (optional)
    sudo pip3 install virtualenv
    mkdir ~/.virtualenvs/
    virtualenv ~/.virtualenvs/bluemedia -p <path>/python3.6

### Configuring App, set local settings
    source ~/.virtualenvs/bluemedia/bin/activate      # if you installed
    pip install -r requirements.txt
    cp bluemedia/local_settings.example.py bluemedia/local_settings.py
    
### Setting up your Django environment
    ./manage.py migrate
    ./manage.py createsuperuser
    ./manage.py loaddata initial_data
    ./manage.py runserver

# TO DO
    make backend url configurable (for now it's need to be 127.0.0.1:8000)
    create tests for frontend
    add files upload
    make views responsive
    use factories is tests (factories are implemented already)
