from django.conf import settings
from django.conf.urls import include
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin

import api.urls

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(api.urls, namespace='api')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
