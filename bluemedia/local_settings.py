DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'bluemedia',
        'USER': 'username',
        'PASSWORD': 'password',
        'HOST': '',
        'PORT': 5432,
    }
}
