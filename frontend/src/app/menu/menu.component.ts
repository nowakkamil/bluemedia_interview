import { Component, OnInit } from '@angular/core';
import { MenuService } from '../services/menu.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: [MenuService]
})
export class MenuComponent implements OnInit {
  menu: any;

  constructor(private activatedRoute: ActivatedRoute, private menuService: MenuService) {
    this.activatedRoute.params.subscribe((params: Params) => {
      const id = params['id'];
      menuService.getMenu(id).subscribe(
        menu => this.menu = menu
      );
          console.log(this.menu);

    });
  }

  ngOnInit() {
  }

}
