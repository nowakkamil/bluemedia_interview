import { Inject, Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class MenuService {

  constructor(@Inject(Http) public http: Http) {}

  getMenuList() {
    return this.http.get('http://127.0.0.1:8000/api/menus/').map((res) => res.json());
  }

  getMenu(id: number) {
    return this.http.get(`http://localhost:8000/api/menus/${id}`).map((res) => res.json());
  }
}
