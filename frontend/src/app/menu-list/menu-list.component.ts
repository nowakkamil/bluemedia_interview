import { Component, OnInit } from '@angular/core';
import { MenuService } from '../services/menu.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css'],
  providers: [MenuService]
})
export class MenuListComponent implements OnInit {
  menus: any;

  constructor(private router: Router, private menuService: MenuService) {
    menuService.getMenuList()
      .subscribe(
        menus => this.menus = menus
      );
  }

  ngOnInit() {
  }

  getRowClass() {
    return {
      'clickable': true
    };
  }

  onActivate(event) {
    return this.router.navigateByUrl(`/menu/${event.row.id}`);
  }
}
