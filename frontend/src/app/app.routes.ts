import {Routes} from '@angular/router';

import {PageNotFoundComponent} from './core/page-not-found/page-not-found.component';
import {MenuListComponent} from './menu-list/menu-list.component';
import {MenuComponent} from './menu/menu.component';

export const appRoutes: Routes = [
    {
        path: '',
        component: MenuListComponent
    },
    {
        path: 'menu/:id',
        component: MenuComponent
    },
    {
      path: '**',
      component: PageNotFoundComponent
    }
];
